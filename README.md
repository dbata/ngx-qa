## @universis/ngx-qa

`@universis/ngx-qa` is an implementation of quality assurance client 
used by universis client applications.

### Installation

    npm i @universis/ngx-qa

### Usage 

Import `QaModule` in app modules

    ...
    import { QaModule } from '@universis/ngx-qa';

    @NgModule({
    declarations: [
        ...
    ],
    imports: [
        ...
        QaModule
    ],
        providers: [
            ...
        ],
        bootstrap: [ AppComponent ]
    })
    export class AppModule {

    }

### Development

This project is intended to be used as a part of an angular cli project. 

- Add this project as git submodule

        git submodule add https://gitlab.com/universis/ngx-qa.git projects/ngx-qa

- Edit angular.json and add `ngx-qa` configuration under `projects` node:

        "ngx-qa": {
            "root": "projects/ngx-qa",
            "sourceRoot": "projects/ngx-qa/src",
            "projectType": "library",
            "prefix": "universis",
            "architect": {
                "build": {
                    "builder": "@angular-devkit/build-ng-packagr:build",
                    "options": {
                        "tsConfig": "packages/ngx-qa/tsconfig.lib.json",
                        "project": "packages/ngx-qa/ng-package.json"
                    }
                },
                "test": {
                "builder": "@angular-devkit/build-angular:karma",
                "options": {
                    "main": "packages/ngx-qa/src/test.ts",
                    "tsConfig": "packages/ngx-qa/tsconfig.spec.json",
                    "karmaConfig": "packages/ngx-qa/karma.conf.js"
                }
                },
                "lint": {
                "builder": "@angular-devkit/build-angular:tslint",
                "options": {
                    "tsConfig": [
                        "packages/ngx-qa/tsconfig.lib.json",
                        "packages/ngx-qa/tsconfig.spec.json"
                    ],
                    "exclude": [
                    "**/node_modules/**"
                    ]
                }
            }
        }
        }

- Import `@universis/ngx-qa` in tsconfig.json#paths

        {
            ...
            "compilerOptions": {
                ...
                "paths": {
                    ...
                    "@universis/ngx-qa": [
                        "packages/ngx-qa/src/public_api"
                    ]
                }
            }
        }
