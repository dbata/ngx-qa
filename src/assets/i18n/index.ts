import {en} from './qa.en';
import {el} from './qa.el';

export const QA_LOCALES = {
    el: el,
    en: en
};
