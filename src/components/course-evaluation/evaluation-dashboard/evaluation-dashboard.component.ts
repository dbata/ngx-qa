import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ErrorService} from '@universis/common';
import {QaService} from '../../../qa.service';

@Component({
  selector: 'universis-evaluation-dashboard',
  templateUrl: './evaluation-dashboard.component.html'
})
export class EvaluationDashboardComponent implements OnInit {

  public evaluations: any = []; // Data
  public isLoading = true;      // Only if data is loaded

  constructor(private _context: AngularDataContext,
              private _qaService: QaService,
              private _errorService: ErrorService) { }

  async ngOnInit() {
    try {
      this.isLoading = true;
      const evaluations = await this._qaService.getCourseClassEvaluations(5);
      this.evaluations = evaluations.map((x) => {
        x.url = `/courses/${x.courseClassInstructor.courseClass.course.id}/${x.courseClassInstructor.courseClass.year.id}/${x.courseClassInstructor.courseClass.period.id}/evaluation`;
        return x;
      });
      // get result for any evaluation
      for (const x of this.evaluations) {
        x.results = await this._qaService.getEvaluationEventTokens(x.id);
      }
    } catch (err) {
      console.error(err);
      return this._errorService.navigateToError(err);
    } finally {
      this.isLoading = false;
    }
  }

}
