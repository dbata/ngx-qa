import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {LoadingService} from '@universis/common';
import {QaService} from '../../../qa.service';

@Component({
  selector: 'universis-evaluation-results',
  templateUrl: './evaluation-results.component.html'
})
export class EvaluationResultsComponent implements OnInit {

  @Input() model: any;
  public results: any;
  public loading = true;

  constructor(private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _qaService: QaService) { }

  ngOnInit() {
    this.loading = true;
    // show loading
    this._loadingService.showLoading();
    // fetch form attributes
    this._qaService.getEvaluationEventAttributes(this.model.id).then(results => {
      // TODO: Get this data formatted from the api
      this.results = results.map(result => {
        if (result.std) {
          result.std = result.std.toFixed(3);
        }
        return result;
      });
      this.loading = false;
      // hide loading
      this._loadingService.hideLoading();
    }).catch(err => {
      // handle forced route id change
      this.results = null;
      // log error
      console.error(err);
      this.loading = false;
      // hide loading
      this._loadingService.hideLoading();
    });
  }
}
