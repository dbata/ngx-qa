import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseOutlineRootComponent } from './course-outline-root.component';

describe('CourseOutlineRootComponent', () => {
  let component: CourseOutlineRootComponent;
  let fixture: ComponentFixture<CourseOutlineRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseOutlineRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseOutlineRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
