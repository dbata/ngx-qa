import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ErrorService, LoadingService, ModalService } from '@universis/common';
import { Observable } from 'rxjs';
// tslint:disable-next-line: max-line-length
import { NewPublicationOrWorkComponent } from './components/instructor-outline/new-publication-or-work/new-publication-or-work.component';

@Injectable({
  providedIn: 'root'
})
export class QaService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _appEventService: AppEventService) { }


  async printReport(id: number, reportParams: any): Promise<Blob> {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const printURL = this._context.getService().resolve(`qa/instructors/me/reports/${id}/print`);
    // get report blob
    return this._http.post(printURL, reportParams, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then((response) => {
      const contentLocation = response.headers.get('content-location');
      if (contentLocation != null) {
        Object.defineProperty(response.body, 'contentLocation', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: contentLocation
        });
      }
      return response.body;
    });
  }

  getReportTemplates(appliesTo) {
    // get available courseOutline report templates
    return this._context.model(`qa/instructors/me/reports`)
      .asQueryable()
      .where('reportCategory/appliesTo').equal(appliesTo)
      .getItems();
  }

  getCourseOutline(courseClass) {
    // fetch CourseOutline info
    return this._context.model('qa/instructors/me/CourseOutlines')
      .where('courseClass').equal(courseClass)
      .expand('generalSkills,courseEvaluations,specificCourseCategory,evaluationLanguages,' +
        'courseLanguages($expand=language),teachingStructureTypes($expand=teachingLearningType),' +
        'courseDigitalContents($expand=eStudyGuideType),courseClass($expand=course($expand=locales),department($expand=locales),period),' +
        'evaluationMethod($expand=locale),ictTypes($expand=locale;$select=id,name),teachingMethods($expand=locale;$select=id,name)')
      .getItem();
  }

  getCourseOutlineSummary(courseClassId) {
    // fetch CourseOutline basic info
    return this._context.model('qa/instructors/me/courseOutlines')
      .select('id,dateCreated,dateModified,courseClass')
      .where('courseClass').equal(courseClassId)
      .expand('courseClass($expand=instructors($filter=instructor eq instructor() and role/alternateName eq \'supervisor\'))')
      .getItem().then(result => {
        if (result) {
          return Promise.resolve(result);
        } else {
          return this._context.model('instructors/me/classes')
            .where('id').equal(courseClassId)
            .expand('instructors($filter=instructor eq instructor() and role/alternateName eq \'supervisor\')')
            .getItem().then(courseClass => {
              const data = { courseClass: courseClass };
              return Promise.resolve(data);
            }).catch(err => {
              return Promise.reject(err);
            });
        }
      }).catch(err => {
        return Promise.reject(err);
      });
  }


  getInstructor() {
    return this._context.model('instructors/me')
      .asQueryable()
      .select('id')
      .getItem().then(result => {
        if (typeof result === 'undefined') {
          return Promise.reject('Instructor data cannot be found');
        }
        return Promise.resolve(result);
      }).catch(err => {
        if (err.status === 404) {
          // throw 401 Unauthorized
          return Promise.reject(Object.assign(new HttpErrorResponse({
            error: new Error('Instructor cannot be found or is inaccessible'),
            status: 401
          }), {
            // assign continue as for auto logout
            continue: '/auth/loginAs'
          }));
        }
        return Promise.reject(err);
      });
  }

  async getCourseClassInstructorEvaluationEvent(courseClass) {
    return this._context.model('/qa/Instructors/me/EvaluationEvents')
      .asQueryable()
      .where('courseClassInstructor/courseClass')
      .equal(courseClass)
      .getItem();
  }

  async getEvaluationEventAttributes(id) {
    return this._context.model(`/qa/Instructors/me/EvaluationEvents/${id}/formAttributes`)
      .asQueryable()
      .getItems();
  }

  getCourseClassEvaluations(take) {
    return this._context.model('qa/Instructors/me/EvaluationEvents')
      .asQueryable()
      .expand('courseClassInstructor($expand=courseClass($expand=course,department,period,year))')
      .take(take)
      .orderByDescending('endDate')
      .getItems();
  }

  getCourseClassEvaluationsYears() {
    return this._context.model('/qa/Instructors/me/EvaluationEvents')
      .asQueryable()
      .select('courseClassInstructor/courseClass/year/alternateName as year')
      .expand('courseClassInstructor($expand=courseClass($expand=course,department,year))')
      .groupBy('courseClassInstructor/courseClass/year/alternateName')
      .orderByDescending('courseClassInstructor/courseClass/year/alternateName')
      .getItems();
  }

  getCourseClassEvaluationsPeriods() {
    return this._context.model('/qa/Instructors/me/EvaluationEvents')
      .asQueryable()
      .select('courseClassInstructor/courseClass/period/alternateName as period')
      .expand('courseClassInstructor($expand=courseClass($expand=course,department,period))')
      .groupBy('courseClassInstructor/courseClass/period/alternateName')
      .orderByDescending('courseClassInstructor/courseClass/period/alternateName')
      .getItems();
  }

  async getInstructorEvaluations(searchText, searchTextYear, searchTextPeriod) {
    return this._context.model('/qa/Instructors/me/EvaluationEvents')
      .asQueryable()
      .where('courseClassInstructor/courseClass/course/displayCode').contains(searchText)
      .or('courseClassInstructor/courseClass/title').contains(searchText)
      .prepare().and('courseClassInstructor/courseClass/year/alternateName').contains(searchTextYear)
      .and('courseClassInstructor/courseClass/period/alternateName').contains(searchTextPeriod)
      .expand('courseClassInstructor($expand=courseClass($expand=course,department,period,year))')
      .getItems();
  }

  getInstructorWorks() {
    return this._context.model('qa/Instructors/me/InstructorWorks')
      .asQueryable()
      .expand('instructorWorkType($expand=locale)')
      .orderByDescending('year')
      .getItems();
  }

  getWorkTypes() {
    return this._context.model('qa/InstructorWorkTypes')
      .asQueryable()
      .select('id', 'alternateName')
      .take(-1)
      .getItems();
  }

  // tslint:disable-next-line: max-line-length
  showPublicationOrWorkModal({ data = null, formName = '', endpoint = '', toastBodySuccess = '', reloadData = () => Promise.resolve(), parseData = input => Promise.resolve(input)} = {}) {
    if (!formName.length || !endpoint.length) { return; }
    try {
      this._modalService.openModalComponent(NewPublicationOrWorkComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          formEditName: formName,
          toastHeader: 'InstructorOutline.CompletedSuccess',
          toastSuccess: toastBodySuccess,
          data: data || {},
          execute: (() => {
            return new Observable((observer) => {
              const component = <NewPublicationOrWorkComponent>this._modalService.modalRef.content;
              const submissionData = component.formComponent.form.formio.data;
              parseData(submissionData)
                .then(parsedData => this._context.model(`qa/Instructors/me/${endpoint}`).save(parsedData))
                .then(() => reloadData().catch((err) => this._errorService.navigateToError(err)))
                .then(() => observer.next())
                .catch(err => observer.error(err));
            });
          })()
        }

      });
    } catch (err) {
      console.log(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async getEvaluationEventTokens(id) {
    return this._context.model(`/qa/Instructors/me/EvaluationEvents/${id}/tokenInfo`)
      .asQueryable()
      .getItems();
  }

}
